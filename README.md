# Mydukan Technology

This repository is for Mydukan assignment problems and associated ressources, if any. <br />
Please contact Mydukan HR for your action items. Following are the assignments whereabouts:

1. Messaging Assignment: <br />
    a. Branch: **backend-assignment01**
    b. Link: https://gitlab.com/sagaranand015/mydukan-assignment/tree/backend-assignment01
    
2. MongoDb Assignment: <br />
    a. Branch: **mongodb-assignment**
    b. Link: https://gitlab.com/sagaranand015/mydukan-assignment/tree/mongodb-assignment
    
